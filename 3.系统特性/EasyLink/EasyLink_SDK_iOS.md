# iOS版本EasyLink SDK
EasyLink SDK的主要功能：

* 将手机端设置的Wi-Fi网络参数发送到设备端
    * 支持EasyLink模式
    * 支持Soft AP模式
    * 支持数据包加密
* 到新入网的设备，并且交互。实现参数设置，OTA等功能

注意，EasyLink模式仅支持在IPv4网络中进行操作。

[EasyLink SDK Source Code](https://code.aliyun.com/mico_app_developer/EasyLink_SDK)


## SDK使用示例程序
1. 简单EasyLink的示例程序：

	1) [简单的示例程序](https://github.com/MXCHIP/EasylinkDemo)：
	
	提供了一个简单的UI，展示EasyLink SDK的使用方法，其中分成3个子UI：
	
    * EasyLink Native：演示了如何使用EasyLink SDK提供的API来完成配置的方法
    * 阿里小智：演示了如何使用基于EasyLink SDK开发的阿里小智标准API来完成配置的方法
    * 机智云：演示了如何使用基于EasyLink SDK开发的机智云标准API来完成配置的方法

	2）[完整UI的示例程序](https://github.com/MXCHIP/EasyLink_iOS)：
	
	提供了比较完整的UI演示了从配网到设备发现，以及配置参数和通讯的主要流程。


* 扫下方二维码， 下载带复杂 UI 的 IOS 版本 Easylink APP。

>特别提示：苹果IOS用户不支持微信扫一扫下载功能，请选用其它扫描二维码功能。
>
>具体使用步骤，可参考：[EasyLink 示例 APP 使用说明](#easylink示例app使用说明)。

![Easylink ios](http://developer.mxchip.com/fileDownload/554) 
	


## API的使用方法
EasyLink的使用流程是：初始化EasyLink实例->设置配置参数->开始发送配网信息->等待新设备发现的回调->停止发送配网信息->如不需要再次配置，销毁EasyLink实例
	
* Step1: 初始化EasyLink实例

    ```
    /**
     @brief 初始化EasyLink配置实例
     @param enable: 是否输出调试信息
     @param	delegate: 提供回掉函数的实例
     @return EasyLink实例.
     */
    - (id)initForDebug:(BOOL)enable WithDelegate:(id)delegate;
    ```
	
* Step2: 设置配置参数
	
    ```
    /**
     @brief 设置配网参数
     @param wlanConfigDict: 网络配置信息字典，其中的字典的Key指和内容参考 wlanConfig key
     @param	userInfo: 设备端可以在EasyLink配网中可以收到的自定义信息，如果没有，请设置成nil
     @param	easyLinkMode: 设置成 EASYLINK_V2_PLUS 或者 EASYLINK_AWS, 和设备中使用的配网方式匹配
     @param	key: 传输的EasyLink报文可以使用RC4进行加密，key就是加密的密码，设备端在开启EasyLink配网时，也需要提供这个密码。
     @return none.
     */
    - (void)prepareEasyLink:(NSDictionary *)wlanConfigDict info:(NSData *)userInfo mode:(EasyLinkMode)easyLinkMode encrypt:(NSData *)key;
    ```

    EasyLink配网模式：easyLinkMode
    
    ```
    typedef enum{
        EASYLINK_V1 = 0,    /**< 不再使用 */
        EASYLINK_V2,        /**< 组播配网 */
        EASYLINK_PLUS,      /**< 广播配网 */
        EASYLINK_V2_PLUS,   /**< 组播+广播配网 */
        EASYLINK_AWS,       /**< 最新的广播配网，提供一键配网中最好的兼容性 */
        EASYLINK_SOFT_AP,   /**< 连接到模块开启的热点进行配网 */
        EASYLINK_MODE_MAX,
    } EasyLinkMode;
    ```
      
    wlanConfig字典内容

    ```
    #define KEY_SSID          @"SSID"               //value type: NSData, 无线网络名称，一定要使用 + (NSData *)ssidDataForConnectedNetwork 方法获得，否则无法正确配置包含中文字符的无线网络 
    #define KEY_PASSWORD      @"PASSWORD"           //value type: NSString or NSData, 无线网络密码
    #define KEY_DHCP          @"DHCP"               //value type: NSNumber(bool), 设备是否自动获取IP地址（开启DHCP），@YES或者@NO， EASYLINK_AWS配网必须用@YES
    #define KEY_IP            @"IP"                 //value type: NSString, IP地址，如果开启DHCP，可以不填，EASYLINK_AWS配网不支持
    #define KEY_NETMASK       @"NETMASK"            //value type: NSString, 子网掩码，如果开启DHCP，可以不填，EASYLINK_AWS配网不支持
    #define KEY_GATEWAY       @"GATEWAY"            //value type: NSString, 网关，如果开启DHCP，可以不填，EASYLINK_AWS配网不支持
    #define KEY_DNS1          @"DNS1"               //value type: NSString, DNS服务器1，如果开启DHCP，可以不填，EASYLINK_AWS配网不支持
    #define KEY_DNS2          @"DNS2"               //value type: NSString, DNS服务器2，如果开启DHCP，可以不填，EASYLINK_AWS配网不支持
    ```


* Step3: 开始发送配网信息
`- (void)transmitSettings;`

* Step4: 等待新设备发现的回调
	如果设备连接到目标网络，SDK可以产生回调，EASYLINK AWS模式中使用UDP广播实现，其余的配网方式使用mDNS实现，服务名称是_easylink_config._tcp。具体的实现方法在SDK中已经实现，应用程序无需关心
	          
    ```
    /**
     @brief 新设备发现回调
     @param client: 客户端编号（可以忽略）
     @param	name: 设备名称，就是设备在mDNS服务中提供的实例名称
     @param	mataDataDict: 元数据，即使设备在mDNS服务中提供的TXT Record，或者UDP广播中提供的JSON数据
     @return none.
     */
    - (void)onFound:(NSNumber *)client withName:(NSString *)name mataData: (NSDictionary *)mataDataDict;
    ```
    	
    如果设备上开启了Config Server功能，那么还会触发onFoundByFTC回调
	     
    ```
    /**
     @brief 新设备发现回调
     @param client: 客户端编号
     @param	name: configDict，设备在Config Server功能中提供的配置信息
     @return none.
     */
    - (void)onFoundByFTC:(NSNumber *)client withConfiguration: (NSDictionary *)configDict;
    ```
	
    如果触发了onFoundByFTC回调，就可以使用`- (void)configFTCClient:(NSNumber *)client withConfiguration: (NSDictionary *)configDict;`方法来设置设备参数了。但是这个功能也要和设备上的Config Server功能配合。	
	
* 	Step5: 停止发送配网信息	
	如果认为配网已经完成，可以停止发送配网信息。
`	- (void)stopTransmitting;`

* 	Step6: 如不需要再次配置，销毁EasyLink实例
	使用这个方法可以彻底地清除easylink示例所占用的资源。
`	- (void)unInit;`




## Easylink示例APP使用说明
准备：

 - 1 个 烧录有 MiCO SDK 开发包中示例程序： wifi_uart 的 MiCO 设备端 或 开发板（[MiCOKit](http://developer.mxchip.com/docs/34) 或 [EMB-380-S2](http://developer.mxchip.com/docs/47)）；
 - 1 个 具备 IOS 系统的智能手机端；
 - 1 个 安装 SecureCRT 软件的 PC 端；


步骤：

1.安装APP： 扫描上方二维码，下载 Easylink APP，并安装。

![eASYLINK icon](http://developer.mxchip.com/fileDownload/310)

2.打开APP：点击界面右上角 “ + ” 图标 ，进入设备配网主界面。


![进入配网页面](http://developer.mxchip.com/fileDownload/312)

3. 开始配网：设备端短按 Easylink 按键，进入重新配网状态。 APP端 输入WiFi的 SSID 和 KEY,如下图，点击快速配网模式：“Fast mode”。



![ssid 和 key](http://developer.mxchip.com/fileDownload/313)  ![配网中](http://developer.mxchip.com/fileDownload/314)

4.设备获取到 SSID 和 KEY 后，APP 即发现设备，如下图，点击 Confirm 即可完成配网。


![配网中](http://developer.mxchip.com/fileDownload/315)

5.点击上图中图标 “ < ” 返回，进入设备列表界面，点击图标“ i 图标即可进入设备信息页面。


![设备列表页面](http://developer.mxchip.com/fileDownload/316)

6.在设备列表中，点击图中" Edit " ， 进入设备信息修改界面。


![设备详情页](http://developer.mxchip.com/fileDownload/323)

7.在修改界面中，用户可重新设置设备IP地址，端口号等信息，如下图，点击 “confirm”即可保存成功。


![设备详情页待编辑](http://developer.mxchip.com/fileDownload/324) ![设备详情页待编辑](http://developer.mxchip.com/fileDownload/325)

设备信息修改后，模块调试串口输出log信息如下：

![设备log](http://developer.mxchip.com/fileDownload/326)

8.点击设备列表中的设备名称，即可进入设备串口透传对话界面，如下图。
**注意：手机 APP 的串口透传演示功能仅支持苹果手机，安卓手机暂不支持。**

![设备列表页面](http://developer.mxchip.com/fileDownload/316)![设备列表页面](http://developer.mxchip.com/fileDownload/319)

注意： 此时，需使用USB转串口工具，将模块的用户串口连接至PC端，并打开PC端串口工具软件，进行收发操作，具体如下图。

![透传设备端](http://developer.mxchip.com/fileDownload/318)

透传信息具体为：
手机 APP 向设备端发送：Hello g55
设备端向手机 APP 发送：hello easylink 


以上是 Easylink 配网过程，用户可以在 MiCO 智能设备中利用 Easylink 功能为设备配网。