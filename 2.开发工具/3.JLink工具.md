# JTAG调试器的使用

JTAG 调试器用于连接 PC 和目标硬件，将 PC 上编译的可执行文件下载到目标硬件上，并且可以对目标硬件进行调试。

```
PC  ==  <USB线>  ==  调试器  ==  <JTAG线>  ==  目标硬件
```

>注：如果目标硬件上已经包含了一个调试器，就只需要将PC和目标硬件用USB线连接起来即可。

在 MiCO 的开发中，可以使用以下调试器：JLink, STLink, DAPLink。有些调试器需要安装驱动程序（详见下文），此外，在编译应用程序时，应使用参数 JTAG=XXX 选择使用的调试器。

|调试器|编译参数|
|:----|:----  |
|JLink SWD接口（默认）|JTAG=jlink_swd|
|JLink JTAG接口|JTAG=jlink|
|STLink|JTAG=stlink-v2或者stlink-v2-1|
|DAPLink|JTAG=cmsis-dap|

例如，在 MiCOKit-3165 板上外接不同的 JTAG 调试器来下载和调试 helloworld 应用，需首先编译命令如下：

```
mico make helloworld@MK3165 download JTAG=jlink_swd     或
mico make helloworld@MK3165 download JTAG=jlink         或
mico make helloworld@MK3165 download JTAG=stlink-v2     或
mico make helloworld@MK3165 download JTAG=stlink-v2-1   
```

## JLink
目前由 MiCO 支持的所有硬件平台均支持 JLink 调试器 （V8 或 V9）。在macOS，Linux平台上将Jlink连接上PC的USB接口后即可使用。在Windows下的安装步骤如下：

1.下载和安装 JLink 驱动程序：[Setup_JLink_V600i.zip](http://developer.mxchip.com/fileDownload/338).安装完成显示：

![jlink原始驱动](/image/5.jtag/jlink.png)

2.下载 Jlink 的 libusb 驱动：[Jlink_Driver_for_MiCO.zip](http://ok0noocp1.bkt.clouddn.com/jlink_driver_for_mico.zip)，解压缩后运行： zadig.exe 或 zadig_xp.exe（用于Windows XP）

![jlink exe path](/image/5.jtag/exe_path.png)

3.选择菜单： Options->List All Devices，下拉列表选择 J-Link，Driver 选择 “libusb-win32（v1.2.6.0）"，点击 "Replace Driver"，返回 "successful" 后即可关闭。在设备管理器中会出现 "libusb-win32 devices。

如果您用的是 JLinkV8，更新过程如下：

![jlinkv8_old](/image/5.jtag/jlink_v8_old.png)   ![jlinkv8_update](/image/5.jtag/jlinkv8_update.png)


如果您用的是 JLinkV9，更新过程如下：

![jlinkv8_old](/image/5.jtag/jlink_v9_old.png)   ![jlinkv9_update1](/image/5.jtag/jlinkv9_update1.png)

![jlinkv9_update2](/image/5.jtag/jlinkv9_update2.png)

>注意：由于不同厂家的 JLink 固件版本不同，导致部分JLink驱动更换为libusb-win32（v1.2.6.0）后可能无法正常工作。这种情况下可尝试使用 "libusbK (v3.0.7.0)"。

![jlink_libusbk_update](/image/5.jtag/jlink_libusbk_update.png)  ![jlink_libusbk_show](/image/5.jtag/jlink_libusbk_show.png)


## STLink
如使用 ST 系列的 MCU作为主控制器，可使用 STLink 仿真器下载和调试。驱动程序下载：[STLINK/V2](http://www.st.com/content/st_com/en/products/development-tools/hardware-development-tools/development-tool-hardware-for-mcus/debug-hardware-for-mcus/debug-hardware-for-stm32-mcus/st-link-v2.html)


 ![stlink_show](/image/5.jtag/stlink_show.png)

## DAPLink
DAPLink 是由ARM提供的开源调试器，支持所有使用Corrtex-M内核的微控制器，且无需安装驱动程序，但是下载和调试的速度比JLink慢。










