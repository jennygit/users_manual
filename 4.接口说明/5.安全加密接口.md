# MiCO Algorithm APIs

MiCO 提供算法包括安全和校验两种。校验算法用来保证数据传输的正确性，完整性和可靠性。安全算法用来保证网络数据传输的安全可信赖性。



数据校验： 

* [CRC8](#crc8)  
* [CRC16](#crc16)

安全加密：

* [MD5](#md5)
* [SHA](#sha)
* [HMAC MD5](#hmac-md5)
* [HMAC SHA](#hmac-sha)
* [DES](#des)
  * [64位密钥的DES加密](#64位密钥的des加密)
  * [192位密钥的DES加密](#192位密钥的des加密) 
* [AES](#aes)
  * [cbc模式](#cbc模式) 
  * [cbc模式的PKCS#5填充AES加密](#cbc模式的pkcs#5填充aes加密) 
  * [ecb模式](#ecb模式) 
* [ARC4](#arc4)
* [Rabbit](#rabbit)
* [RSA](#rsa)

本文仅梳理 MiCO 算法的主要 API 框架及部分示例。更详细说明及实现,可参考完整的 [doxygen](http://developer.mico.io/api/) 文档，或查阅 [MiCO Demos](https://code.aliyun.com/mico/mico-demos.git) 。 


## CRC8
用于系统数据传输时的 CRC8 校验功能实现。

* [CRC8_Init](#CRC8_Init)：初始化一个 CRC8 结构体
* [CRC8_Update](#CRC8_Update)：计算 CRC8 的值
* [CRC8_Final](#CRC8_Final)：输出CRC8结果


## CRC16
用于系统数据传输时的 CRC16 校验功能实现。

* [CRC16_Init](#CRC16_Init)：初始化一个 CRC16 结构体
* [CRC16_Update](#CRC16_Update)：计算 CRC16 的值
* [CRC16_Final](#CRC16_Final)：输出 CRC16 结果


## MD5
用于系统数据传输时的 MD5 哈希值的生成功能。

* [InitMd5](#InitMd5)：创建一个 MD5 结构体
* [Md5Update](#Md5Update):更新 MD5 处理缓冲区
* [Md5Final](#Md5Final)：输出 MD5 最终摘要内容 

## SHA
用于系统数据传输时的 SHA 哈希值的生成，包括 4 种： SHA1，SHA
256，SHA384，SHA512。

* [USHAReset](#USHAReset)：初始化一个SHAContext, 准备计算一个新的SHA消息摘要。
* [USHAInput](#USHAInput)：将八位的字节数组作为消息的下一部分
* [USHAFinalBits](#USHAFinalBits)：添加消息的任意最终位
* [USHAResult](#USHAResult)：返回具有适当位数的消息摘要，至回调函数的数组
* [USHABlockSize](#USHABlockSize)：返回给定的SHA算法的块大小
* [USHAHashSize](#USHAHaashSize)：返回给定的SHA算法的哈希大小
* [USHAHashSizeBits](#USHAHashSizeBits)：返回给定的SHA算法的哈希大小，以位表示
* [USHAHashName]()：返回SHA算法名称的字符串


## HMAC MD5
用于系统数据传输时 HMAC_MD5 摘要值的生成。

* [HmacSetKey](#HmacSetKey)：扩展key为一个更长的字符串
* [HmacUpdate](#HmacUpdate)：生成过程缓存
* [HmacFinal](#HmacFinal)：最终摘要内容 输出


## HMAC SHA
用于系统数据传输时 HMAC_SHA 摘要值的生成。

* [hmacReset](#hmacReset): 初始化一个存放新的HMAC消息摘要值的结构体
* [hmacInput](#hmacInput): 接受一个八位字节数组作为下一个部分的消息
* [hmacResult](#hmacResult): 返回N个字节的消息摘要 至 调用函数定义的数组中


## DES
用于加密或解密 DES 的密钥。

### 64位密钥的DES加密
* [Des_SetKey](#Des_SetKey)：DES 扩展外部密钥为一个长字符串
* [Des_CbcEncrypt](#Des_CbcEncrypt)：cbc模式的 DES 加密过程
* [Des_CbcDecrypt](#Des_CbcDecrypt)：cbc模式的 DES 解密过程


### 192位密钥的DES加密
* [Des3_SetKey](#Des3_SetKey)：DES3 扩展外部密钥为一个长字符串
* [Des3_CbcEncrypt](#Des3_CbcEncrypt)：cbc模式的 DES3 加密过程
* [Des3_CbcDecrypt](#Des3_CbcDecrypt)：cbc模式的 DES3 解密过程


## AES
用于实现 AES 加密或解密功能。

### cbc模式

* [AesSetKey]()：AES扩展外部密钥为一个长字符串 
* [AesCbcEncrypt]()：cbc模式的AES加密过程
* [AesCbcEncrypt]()：cbc模式的AES解密过程


### cbc模式的PKCS#5填充AES加密

* [AesCbcEncryptPkcs5Padding]()：cbc模式PKCS#5填充的AES加密过程
* [AesCbcDecryptPkcs5Padding]()：cbc模式PKCS#5填充的AES解密过程 



### ecb模式

* [AesSetKeyDirect]()：AES扩展外部密钥为一个长字符串 
* [AesEncryptDirect]()：ecb模式的AES加密过程
* [AesDecryptDirect]()：ecb模式的AES加密过程



## ARC4
用于实现 ARC4 的加解密功能。

* [Arc4SetKey]()：Arc4 扩展外部密钥为一个较长的字符串 
* [Arc4Process]()：ARC4 加密或解密过程

## Rabbit
用于实现 Rabbit 的加解密功能。

* [RabbitSetKey]()：Rabbit扩展外部密钥为一个较长的字符串 
* [RabbitProcess]()：Rabbit加密或解密过程


## RSA
用于实现 RSA 的加密和解密功能。

* [InitRsaKey]()：初始化RsaKey
* [FreeRsaKey]()：释放RsaKey
* [InitRng]()：为RSA，初始化Rng
* [RsaPublicEncrypt]()：使用公共密钥加密字符串
* [RsaPrivateDecrypt]()：使用私用密钥解密
* [RsaSSL_Sign]()：使用RSA签名数据
* [RsaSSL_Verify]()：使用RSA验证数据
* [RsaPrivateKeyDecode]()：解码私钥
* [RsaPublicKeyDecode]()：解码公钥











