# 第三方开发工具及驱动

本文主要介绍：基于 MiCO 系统二次开发物联网智能设备时，需要用到的第三方工具及驱动程序。

## 目录

1. [串口转换工具-TTL转USB模块](#串口转换工具-ttl转usb模块)
2. [micro-usb串口驱动](#mico-usb串口驱动)
3. [串口调试软件](#串口调试软件)
    1. [SecureCRT](#securecrt)
    2. [格西烽火](#格西烽火)
3. [Socket 调试工具](#socket-调试工具)
4. [MQTT 服务测试工具](#mqtt-服务测试工具)

## 串口转换工具 TTL转USB
当使用 MiCO 用户串口进行开发调试时，通常需要外接 TTL转USB 模块，与 PC 连接进行调试，这里推荐使用：[CP2102 USB Bridge Driver](http://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)。
安装后，即可通过 PC 端串口调试软件，连接对应的 COM 端口，进行串口通信调试。

---

## micro usb串口驱动
MiCOKit 开发板的 micro-usb 调试串口log信息查看，需要 PC 端安装驱动软件，可至： 
[FTDI_D2XX Driver](http://www.ftdichip.com/Drivers/D2XX.htm) 页面，根据您的 PC 系统类型下载。

## 串口调试软件
### SecureCRT
为了查看 MiCO 的调试或用户串口信息，需在 PC 端安装串口调试软件，这里推荐下载：[SecureCRT](https://www.vandyke.com/download/securecrt/download.html)。  
该软件还支持 “Ymodem 协议” ，可在 MiCO 的  [Bootloader 模式](/3.MiCO_system_performance/1.MiCO_bootloader.md) 进行固件更新等操作。


### 格西烽火
针对 MiCO AT 指令的开发与调试，推荐使用一款简单易用的串口调试辅助软件：[格西烽火](http://www.geshe.com/zh-cn/products/gbeacon)。

同时，为了让开发者快速进行 AT 指令的开发与调试，这里提供一个已经集成好的： [MiCO AT 指令格西烽火 bsp 工程](http://developer.mico.io/fileDownload/339) ，格西烽火软件安装后，即可直接双击打开使用。



## Socket 调试工具
MiCO 为开发者提供的基本的 TCP、UDP通信服务接口，开发时通常要用到 Socket 通信调试工具，推荐一个soket 调试工具下载链接：[TCPUDPDebug102_Setup.exe](http://developer.mico.io/fileDownload/340)。



## MQTT 服务测试工具
MiCO 设备连接的一些云服务支持 MQTT协议，开发者通常需要搭建一个 MQTT客户端进行测试，这里推荐一个 MQTT客户端测试工具下载链接：[MQTT.fx](http://www.mqttfx.org/)。 

具体的使用方法，可点击查看：[MQTT Client 视频教程 ](http://edu.mico.io/lesson/24/35)。












