# 针对 md文件的 MarkdownPad2 使用说明
  
1. [简介](#简介)
2. [下载MarkdownPad2](#下载-markdownpad2)
3. [授权MarkdownPad2](#授权-markdownpad2)
4. [Markdown语法简要规则](#markdown-语法简要规则)
	1. [标题](#标题)
	2. [列表](#列表)
	3. [引用](#引用)
	4. [插入图片和链接](#插入图片和链接)
	5. [粗体与斜体](#粗体与斜体)
	6. [表格](#表格)
	7. [代码块](#代码块)
	8. [分割线](#分割线)
	9. [目录](#添加目录)
5. [结束语](#结束语)
6. [写在最后](#写在最后)



## 简介
这是 MiCO 官方网站 mico.io 文档中心 - handbook 页面 .md 文件库，这里所有的 md 文件都是使用 MarkdownPad2 编辑器进行编辑的，采用简单易学的 Markdown 语法进行书写。

如果您是 MiCO 开发人员，也可以一起参与 handbook 内容的建设，在此仓库上进行 md 文档的书写与提交。具体使用步骤：

* 1.让管理员添加您至 Git 开发组 -  developer；
* 2.`git clone git@code.aliyun.com:mico/mico-handbook.git`,  克隆 handbook 仓库到本地；
* 3.`git checkout -b branch1`, 创建一个分支并切换至该分支；
* 4.`git add .`, 添加您当前分支修改至工作区；
* 5.`git commit -m "add a file"`, 提交至本地仓库；
* 6.`git push git@code.aliyun.com:mico/mico-handbook.git branch1`，提交本地修改至远程branch1分支。


## 下载 MarkdownPad2

您可以至：[MarkdownPad2](http://markdownpad.com/) 下载安装程序，并根据页面提示进行安装。

>注意： Win10 操作系统需安装必要的插件才能正常使用。 

>下载地址1：[awesomium_v1.6.6_sdk_win.exe](http://markdownpad.com/download/awesomium_v1.6.6_sdk_win.exe) ，点击链接下载

>下载地址2：公司服务器文件夹，路径：\\192.168.2.24\mico 公用\工具软件下载\awesomium_v1.6.6_sdk_win.exe.

## 授权 MarkdownPad2 
之前安装的 MarkdownPad2 是免费版本，缺少列表功能。这里需要授权处理以增加列表功能，步骤：

1.添加表格的扩展：工具 -> 选项 -> Markdown -> Markdown处理器 改为 “Markdown(扩展)”即可。

2.在修改过程中，软件会自动提示您注册：MarkdownPad2:

<pre>
邮箱：
Soar360@live.com
授权秘钥：
GBPduHjWfJU1mZqcPM3BikjYKF6xKhlKIys3i1MU2eJHqWGImDHzWdD6xhMNLGVpbP2
M5SN6bnxn2kSE8qHqNY5QaaRxmO3YSMHxlv2EYpjdwLcPwfeTG7kUdnhKE0vVy4RidP6
Y2wZ0q74f47fzsZo45JE2hfQBFi2O9Jldjp1mW8HUpTtLA2a5/sQytXJUQl/QKO0jUQY4pa5C
Cx20sV1ClOTZtAGngSOJtIOFXK599sBr5aIEFyH0K7H4BoNMiiDMnxt1rD8Vb/ikJdhGMMQr
0R4B+L3nWU97eaVPTRKfWGDE8/eAgKzpGwrQQoDh+nzX1xoVQ8NAuH+s4UcSeQ==    
</pre>

注意：

* 预览 md 文件的表格格式，请切换编辑器模式，点击左下角 "M" 符号，切换至：Markdown(Extra)
* 预览 md 文件的代码格式，请切换编辑器模式，点击左下角 "M" 符号，切换至：CommonMark


## Markdown 语法简要规则

这里简要介绍一些较为常用格式的语法规则，更为详细和高级的语法可参考：[快速了解 Markdown 语法](http://www.appinn.com/markdown/) 。通过以下几个基本用法，手把手教您 如何快速将 MarkdownPad2 编辑器 玩起来，请用心读哦。

**友情提醒：**请特别关注以下每个语法格式后的注意内容，否则容易使用错误的格式。



### 标题
标题是文章中最常用的格式，在 Markdown 中，如果一段文字被定义为标题，只要在这段文字前加 # 号即可。如写成：
<pre># 一级标题
## 二级标题
### 三级标题
</pre>

显示效果：
># 一级标题
>## 二级标题
>### 三级标题

**注意：每个标题的最后一个#号后需加一个空格，否则显示不正确。**

以此类推，总共六级标题，建议在井号后加一个空格，这是最标准的 Markdown 语法。

### 列表 

#### 无序列表
文字前添加 * 或 - 即可，如写成：
<pre>
* 无序列表行 1
* 无序列表行 2
* 无序列表行 3
</pre>
或
<pre>
- 无序列表行 1
- 无序列表行 2
- 无序列表行 3
</pre>

显示效果：

* 无序列表行 1
* 无序列表行 2
* 无序列表行 3

和

- 无序列表行 1
- 无序列表行 2
- 无序列表行 3

**注意：符号 * 或 - ，要和文字之间加一个空格。**

#### 有序列表
文字前直接添加1. 2. 3.，如写成：
<pre>
1. 有序列表行 1
2. 有序列表行 2
3. 有序列表行 3
</pre>
显示效果：

1. 有序列表行 1
2. 有序列表行 2
3. 有序列表行 3


**注意：符号要和文字之间加上一个字符的空格。**

### 引用

如果你需要引用一小段别处的句子，那么就要用引用的格式。只需要在文本前加入 > 这种尖括号（大于号）即可，如写成：
<pre>> 引用行 1  
> 引用行 2  
> 引用行 3
</pre>

显示效果：

> 引用行 1  
> 引用行 2  
> 引用行 3  


**注意：每行后面必须多两个空格，才能重启一行，如：引用行 1空格空格。**

### 插入图片和链接

插入链接与插入图片的语法很像，区别在一个 ! 号。

#### 图片  
##### web 服务器图片
外部图片为(来自 web 服务器)，如写成：
<pre>
![Mou icon](http:mouapp.com/Mou_128.png)
</pre>


显示效果：

![Mou icon](http:mouapp.com/Mou_128.png)

##### 本地目录图片

内部图片为(必须来自本 md 文件当前目录下的图片或文件夹中的图片，否则无法正确显示)，如写成：
<pre>
![my_picture](image/my_picture.png)
</pre>

显示效果：

![my_picture](image/my_picture.png)

> 注意：图片的路径中绝不允许出现汉字或空格，包括md文件路径及名称，也不可以出现汉字或空格。
> 这里建议将 md 文件使用的图片，放置于当前目录的 image 文件夹中，也可再建一级子文件夹以便快速按类查找。如：

<pre>
![my_picture](image/test/my_picture.png)
</pre>

显示效果：

![my_picture](image/test/my_picture.png)

## 链接

### 插入web链接地址
写成：

<pre>
[百度一下](http://baidu.com)
</pre>

显示效果： [百度一下](http://baidu.com)

### 插入相对路径地址
写成：（如插入本handbook某页面链接）

<pre>
[MiCO_SDK 下载页面](/Download/1.MiCO_SDK.md)
</pre>

显示效果：[MiCO_SDK 下载页面](/Download/1.MiCO_SDK.md)

### 插入相对路径的页面某目录内容
写成：（如插入本handbook某页面中某标题位置链接）

<pre>
[AT HELP 命令](/AT_command/8.AT_command_detail.md#athelp)
</pre>

显示效果：[AT HELP 命令](/AT_command/8.AT_command_detail.md#athelp)



### 粗体与斜体

Markdown 的粗体和斜体非常简单，用两个 * 包含一段文本就是粗体的语法，用一个 * 包含一段文本就是斜体的语法。

#### 粗体
如写成：
<pre>
**这是粗体**
</pre>

显示效果：

**这是粗体**


#### 斜体

<pre>
*这是斜体*
</pre>

显示效果：

*这是斜体*


### 表格

表格是我觉得 Markdown 比较累人的地方，如写成：

<pre>
| Tables        | Are           | Cool  |
| :---          |:---:          |   ---:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
</pre>

显示效果：

| Tables        | Are           | Cool  |
| :---          |:---:          | ---:  |
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

<pre>
其中：
:---    冒号在左边，表示：左对齐
:---:   冒号在两边，表示：居中对齐
---：   冒号在右边，表示：右对齐
(注意：符合 “-”的数量必须 大于等于 3 )
</pre>

### 代码块

如果你是个程序猿，需要在文章里优雅的引用代码框，在 Markdown下实现也非常简单。

#### 单行代码

只需要用两个 ` 把中间的代码包裹起来。如写成：

<pre>
`我是不换行的单行代码，快来读我。'
</pre>

显示效果：`我是第一行代码，快来读我。`

>注意：如需另起一行，需前后空一行。

<pre>

`我是换行的单行代码，快来读我。'
</pre>

显示效果：

`我是第一行代码，快来读我。`


#### 多行代码
当有多行代码时，需要使用  ``` 符合，如写成：
>注意：在 ``` 的前后，均需要空一行，否则显示不正常。

<pre><code>

```
# include 
# include
　 int main(void)
  {...}
```

</code></pre>

显示效果：

```
 #include 
 #include
 int main()
 {...}
```


当使用该符合，代码格式显示仍不正确时，您还可以使用：

![code_frame](image/code_frame.png)

显示效果：
<pre><code>  代码内容   </code></pre>

**注意：使用 tab 键即可缩进代码。**


### 分割线
分割线的语法只需要三个 * 或 - 号，如写成：

<pre>
  ***
  ---
</pre>

显示效果：

***
---

### 添加目录
如果您的 md 文件内容较长，可以在头部添加目录，如写成：
<pre>1. [标题1](#标题1)
2. [标题2](#标题2)
 1. [子标题1](#子标题1)
 2. [子标题2](#子标题2)
 3. [子标题3](#子标题3)
3. [标题3](#标题3)
</pre>

显示效果：

1. [标题1](#标题1)
2. [标题2](#标题2)
 1. [子标题1](#子标题1)
 2. [子标题2](#子标题2)
 3. [子标题3](#子标题3)
3. [标题3](#标题3)

**需要特别注意的是**：
>
>括号中的标题文字内容，英文字母全部采用小写格式；
>
>括号中 # 后不允许添加空格，若标题中有空格，需用中划线 - 代替。
>
>括号中 # 后不允许添加中划线 -  符号，否则链接不正确，因此标题中不允许使用中划线 - 符号，但可以使用下划线 `_`。
>
>括号中的标题 # 后，若标题中出现点 `.`，加号 `+`，等号 `=`，这几个符号， 必须全部省略不写，否则链接不正确。



## 结束语
到这里，Markdown 的基本语法在日常的使用中基本就没什么大问题了，只要多加练习，配合好用的工具，写起东西来肯定会行云流水。



## 写在最后
更多更高级的 Markdown 语法规则 可参考：[Markdown 语法说明](http://www.appinn.com/markdown/)



